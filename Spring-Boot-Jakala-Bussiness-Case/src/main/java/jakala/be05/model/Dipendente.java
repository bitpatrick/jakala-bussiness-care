package jakala.be05.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import jakala.be05.exception.DipendenteException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity//(name = "dipendenti")
//@Table(name = "dipendenti")
public class Dipendente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@NotBlank
	private String nome;
	
	@OneToOne
	private Supermercato supermercato;
	
	@ManyToOne
	private Ortofrutta ortofrutta;
	
	@ManyToOne
	private Salumeria salumeria;
	
	@ManyToOne
	private Magazzino magazzino;
	
	@ManyToOne
	private Cassa cassa;

	
	public Dipendente(Supermercato supermercato) {
		super();
		this.supermercato = supermercato;
	}
	
	
	
	


	
	

}
