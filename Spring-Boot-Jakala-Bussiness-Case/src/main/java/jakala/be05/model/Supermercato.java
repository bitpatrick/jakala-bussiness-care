package jakala.be05.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity(name = "supermercati")
@Table(name = "supermercati")
public class Supermercato {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	@OneToOne
	private Ortofrutta ortofrutta;
	
	@OneToOne
	private Salumeria salumeria;
	
	@OneToOne
	private Magazzino magazzino;
	
	@OneToOne
	private Cassa cassa;
	
	
	
	
	

	
	
	
	
	
	
	

}
