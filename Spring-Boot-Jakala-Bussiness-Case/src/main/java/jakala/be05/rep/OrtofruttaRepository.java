package jakala.be05.rep;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jakala.be05.model.Ortofrutta;

@Repository
public interface OrtofruttaRepository  extends JpaRepository<Ortofrutta, Long>{

}
