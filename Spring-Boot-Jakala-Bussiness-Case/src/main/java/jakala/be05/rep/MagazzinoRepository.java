package jakala.be05.rep;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jakala.be05.model.Magazzino;

@Repository
public interface MagazzinoRepository extends JpaRepository<Magazzino, Long>{

}
