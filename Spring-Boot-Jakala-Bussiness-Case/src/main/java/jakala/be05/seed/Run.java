package jakala.be05.seed;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import jakala.be05.config.MyApplicationContextConfiguration;
import jakala.be05.model.Cassa;
import jakala.be05.model.Dipendente;
import jakala.be05.model.Ortofrutta;
import jakala.be05.model.Reparto;
import jakala.be05.model.Supermercato;
import jakala.be05.rep.CassaRepository;
import jakala.be05.rep.DipendenteRepository;
import jakala.be05.rep.MagazzinoRepository;
import jakala.be05.rep.OrtofruttaRepository;
import jakala.be05.rep.SalumeriaRepository;
import jakala.be05.rep.SupermercatoRepository;
import jakala.be05.service.DipendenteService;


@Component
public class Run implements CommandLineRunner {
	
	Logger LOG = LoggerFactory.getLogger(Run.class);
	
	@Autowired
	SupermercatoRepository supermercatoRepository;
	
	@Autowired
	private OrtofruttaRepository ortofruttaRepository;
	
	@Autowired
	private SalumeriaRepository salumeriaRepository;
	
	@Autowired
	private MagazzinoRepository magazzinoRepository;
	
	@Autowired
	private CassaRepository cassaRepository;
	
	@Autowired
	private DipendenteRepository dipendenteRepository;
	
	@Autowired
	private DipendenteService dipendenteService;
	
	@Override
	public void run(String... args) throws Exception {
		
		LOG.info("run method of CommandLineRunner is running...");
		
		ApplicationContext ctx = new AnnotationConfigApplicationContext(MyApplicationContextConfiguration.class);
		
		/////////////////CREAZIONE OGGETTO SUPERMERCATO//////////////////////////////
		Supermercato supermercatoPewex = (Supermercato) ctx.getBean("supermercatoPewex");
		//salvataggio oggetto supermercato nel database
		saveSupermercatoInDatabase(supermercatoPewex);
		
		//////////////////CREAZIONE DIPENDENTI/////////////////////////////
		Dipendente dipendentePewex1 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex1.setNome("Marco");
		dipendentePewex1.setOrtofrutta(supermercatoPewex.getOrtofrutta());
		dipendenteRepository.save(dipendentePewex1);
		
		Dipendente dipendentePewex2 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex2.setNome("Teresa");
		dipendentePewex2.setCassa(supermercatoPewex.getCassa());
		dipendenteRepository.save(dipendentePewex2);
		
		Dipendente dipendentePewex3 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex3.setNome("Paolo");
		dipendentePewex3.setOrtofrutta(supermercatoPewex.getOrtofrutta());
		dipendentePewex3.setSalumeria(supermercatoPewex.getSalumeria());
		dipendenteRepository.save(dipendentePewex3);
		
		Dipendente dipendentePewex4 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex4.setNome("Lucia");
		dipendentePewex4.setMagazzino(supermercatoPewex.getMagazzino());
		dipendentePewex4.setCassa(supermercatoPewex.getCassa());
		dipendenteRepository.save(dipendentePewex4);
		
		Dipendente dipendentePewex5 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex5.setNome("Giorgo");
		dipendentePewex5.setSalumeria(supermercatoPewex.getSalumeria());
		dipendentePewex5.setMagazzino(supermercatoPewex.getMagazzino());
		dipendenteRepository.save(dipendentePewex5);
		
		Dipendente dipendentePewex6 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex6.setNome("Nicola");
		dipendentePewex6.setMagazzino(supermercatoPewex.getMagazzino());
		dipendenteRepository.save(dipendentePewex6);
		
		Dipendente dipendentePewex7 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex7.setNome("Silvia");
		dipendentePewex7.setSalumeria(supermercatoPewex.getSalumeria());
		dipendenteRepository.save(dipendentePewex7);
		
		//Marco passa al magazzino
		dipendenteService.cambioReparto(dipendentePewex1, supermercatoPewex.getOrtofrutta(), supermercatoPewex.getMagazzino());
		
		//Lucia lascia la cassa e passa all'ortofrutta
		dipendenteService.cambioReparto(dipendentePewex4, supermercatoPewex.getCassa(), supermercatoPewex.getOrtofrutta());
		
		//viene assunto Stefano
		Dipendente dipendentePewex8 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex8.setNome("Stefano");
		Set<Reparto> repartiDipendentePewex8 = new HashSet<Reparto>();
		repartiDipendentePewex8.add(supermercatoPewex.getMagazzino());
		dipendenteService.assunzione(dipendentePewex8, repartiDipendentePewex8);
		
		//viene assunto Federico
		Dipendente dipendentePewex9 = (Dipendente) ctx.getBean("dipendentePewex");
		dipendentePewex9.setNome("Federico");
		Set<Reparto> repartiDipendentePewex9 = new HashSet<Reparto>();
		repartiDipendentePewex9.add(supermercatoPewex.getSalumeria());
		dipendenteService.assunzione(dipendentePewex9, repartiDipendentePewex9);
		
		
		
		
		
	}
	
	private void saveSupermercatoInDatabase(Supermercato supermercato) {
		ortofruttaRepository.saveAndFlush(supermercato.getOrtofrutta());
		salumeriaRepository.saveAndFlush(supermercato.getSalumeria());
		magazzinoRepository.saveAndFlush(supermercato.getMagazzino());
		cassaRepository.saveAndFlush(supermercato.getCassa());
		supermercatoRepository.saveAndFlush(supermercato);
	}
	
	
	
	


	
	

}
