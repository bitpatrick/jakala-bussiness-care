package jakala.be05.restcontroller;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jakala.be05.model.Dipendente;
import jakala.be05.service.DipendenteService;

@RestController
@RequestMapping("/api")
public class RestContoller {
	
	static final Logger LOG = LoggerFactory.getLogger(RestContoller.class);
	
	@Autowired
	DipendenteService dipendenteService;
	
	@GetMapping("/listaDipendenti")
	public ResponseEntity<List<Dipendente>> getListaDipendenti() {
		
		List<Dipendente> listaRecuperata = dipendenteService.listaDipendenti();
		
		ResponseEntity<List<Dipendente>> responseEntity = new ResponseEntity<List<Dipendente>>(listaRecuperata,HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping("/test")
	public String test() {
		return "test";
	}
	
	

}
