package jakala.be05.service;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jakala.be05.exception.DipendenteException;
import jakala.be05.model.Cassa;
import jakala.be05.model.Dipendente;
import jakala.be05.model.Magazzino;
import jakala.be05.model.Ortofrutta;
import jakala.be05.model.Reparto;
import jakala.be05.model.Salumeria;
import jakala.be05.rep.DipendenteRepository;

@Service
public class DipendenteService {
	
	@Autowired
	private DipendenteRepository dipendenteRepository;
	
	public void cambioReparto(Dipendente dipendente, Reparto vecchioReparto, Reparto nuovoReparto) {
		
		//cancellazione vecchio reparto
		if ( vecchioReparto instanceof Ortofrutta ) {
			
			dipendente.setOrtofrutta(null);
	
		} else if ( vecchioReparto instanceof Salumeria ) {
			
			dipendente.setSalumeria(null);
		
		} else if ( vecchioReparto instanceof Magazzino ) {
			
			dipendente.setMagazzino(null);
		
		} else if ( vecchioReparto instanceof Cassa ) {
			
			dipendente.setCassa(null);
		}	
		
		else {
			throw new DipendenteException("impossibile cancellare il vecchio reparto ");
		}
		
		// inserimento nuovo reparto
		if ( nuovoReparto instanceof Ortofrutta ) {
			
			dipendente.setOrtofrutta((Ortofrutta) nuovoReparto);	
	
		} else if ( nuovoReparto instanceof Salumeria ) {
			
			dipendente.setSalumeria((Salumeria) nuovoReparto);
		
		} else if ( nuovoReparto instanceof Magazzino ) {
			
			dipendente.setMagazzino((Magazzino) nuovoReparto);
		
		} else if ( nuovoReparto instanceof Cassa ) {
			
			dipendente.setCassa((Cassa) nuovoReparto);
		}	
		
		else {
			throw new DipendenteException("impossibile trasferire il dipendente al nuovo reparto");
		}
		
		dipendenteRepository.save(dipendente);
	
	}
	
	public void assunzione(Dipendente dipendente, Set<Reparto> reparti) {
		
		
		for ( Reparto reparto : reparti) {
			
			if ( reparto instanceof Ortofrutta ) {
				
				dipendente.setOrtofrutta((Ortofrutta) reparto);	
		
			} else if ( reparto instanceof Salumeria ) {
				
				dipendente.setSalumeria((Salumeria) reparto);
			
			} else if ( reparto instanceof Magazzino ) {
				
				dipendente.setMagazzino((Magazzino) reparto);
			
			} else if ( reparto instanceof Cassa ) {
				
				dipendente.setCassa((Cassa) reparto);
			}	
			
			else {
				throw new DipendenteException("impossibile trasferire il dipendente al nuovo reparto");
			}
			
			
		}
		
		dipendenteRepository.save(dipendente);
	
	}
	
	public List<Dipendente> listaDipendenti() {
		return dipendenteRepository.findAll();
	}

	
	
	
	

}
