package jakala.be05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJakalaBussinessCaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJakalaBussinessCaseApplication.class, args);
	}

}
