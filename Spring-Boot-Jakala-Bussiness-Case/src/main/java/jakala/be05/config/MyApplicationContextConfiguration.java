package jakala.be05.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jakala.be05.model.Cassa;
import jakala.be05.model.Dipendente;
import jakala.be05.model.Magazzino;
import jakala.be05.model.Ortofrutta;
import jakala.be05.model.Salumeria;
import jakala.be05.model.Supermercato;

@Configuration
public class MyApplicationContextConfiguration {
	
	@Bean
	public Supermercato supermercatoPewex() {
		Supermercato supermercato = Supermercato.builder()
				.nome("Supermercato Pewex")
				.cassa(cassa())
				.magazzino(magazzino())
				.ortofrutta(ortofrutta())
				.salumeria(salumeria())
				.build();
		return supermercato;
		
	}
	
	@Bean
	public Ortofrutta ortofrutta() {
		return new Ortofrutta();
	}
	
	@Bean
	public Magazzino magazzino() {
		return new Magazzino();
	}
	
	@Bean
	public Salumeria salumeria() {
		return new Salumeria();
	}
	
	@Bean
	public Cassa cassa() {
		return new Cassa();
	}
	
	@Bean
	public Dipendente dipendentePewex() {
		Dipendente dipendente = Dipendente.builder()
				.supermercato(supermercatoPewex())
				.build();
		return dipendente;
	}
	
	
	
	

}
	